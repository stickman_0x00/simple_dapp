// Doesn't work on firefox.
// import abi from "./abi.json" assert { type: "json" };
const abi = [
	{
		"inputs": [],
		"name": "get_text",
		"outputs": [{
			"internalType": "string",
			"name": "",
			"type": "string"
		}],
		"stateMutability": "view",
		"type": "function"
	},
	{
		"inputs": [{
			"internalType": "string",
			"name": "text",
			"type": "string"
		}],
		"name": "set_text",
		"outputs": [],
		"stateMutability": "nonpayable",
		"type": "function"
	}
];

export default abi;