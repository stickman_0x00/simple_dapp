import contract from "./contract/contract.js";

async function show_text() {
	document.getElementById("saved_text").textContent = "Text saved:\n" + await contract.get_text();
}

async function set_text() {
	await contract.set_text(document.getElementById("text").value);
	show_text();
}

async function get_text_account() {
	const account = document.getElementById("account").value;
	document.getElementById("saved_text").textContent = "Text saved:\n" + await contract.get_text_of_account(account);
}

document.body.onload = show_text;
document.getElementById("get_text").addEventListener("click", show_text);
document.getElementById("get_text_account").addEventListener("click", get_text_account);
document.getElementById("set_text").addEventListener("click", set_text);



// Hide/Show Advanced options
const advanced = document.getElementById("advance_options");
advanced.style.display = "none";
document.getElementById("btn_advanced").addEventListener("click", function(){
	if (advanced.style.display === "none") {
		advanced.style.display = "";
		return;
	}

	advanced.style.display = "none";
});