import abi from "../abi/abi.js";

const contract_address = "0xc75ead8a5F08789bbA58C2f1F9fb787A01e0b354";

// Request access to the user's MetaMask account.
async function request_account() {
	console.log("request_account");
	// Check if metamask exist (check if window ethereum exist)
	if (!window.ethereum) {
		rejection("Install or activate metamask.");
		return;
	}

	await window.ethereum.request({ method: "eth_requestAccounts" });
}


function get_contract() {
	let web3 = new Web3(window.ethereum);
	return new web3.eth.Contract(abi, contract_address);
}

// Return connected account.
async function get_account() {
	let web3 = new Web3(window.ethereum);
	const accounts = await web3.eth.getAccounts();

	return accounts[0];
}

// Get text of current account selected.
async function get_text() {
	console.log("get_text");
	request_account();

	const account = await get_account();

	return get_contract().methods
		.get_text()
		.call({ from: account });
}

async function get_text_of_account(account) {
	console.log("get_text");
	request_account();

	return get_contract().methods
		.get_text()
		.call({ from: account });
}

const set_text = async () => {
	request_account();

	const account = await get_account();

	// Get accounts
	return get_contract().methods
		.set_text(document.getElementById("text").value)
		.send({ from: account })
		.then((text) => {
			console.log(text);
		});
};

export default { get_text, get_text_of_account, set_text };